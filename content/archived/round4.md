---
title: "Round4"
date: 2020-03-10T19:55:15-03:00
draft: false
---

# Round 4 - Campinas/Brazil (On Going - 1o Sem 2020)

NOTE: Due to COVID-19, we are holding remote meetings since April 14. Please get in touch for more information.

### Attending the meetings
- **When:** Every Tuesday at 19h30 (Sao Paulo time) starting on 10 Mar 2020.
- **Where:** Remote (links are published in the mailing list or IRC/Telegram channel just before)
- **Register** to attend the meetings through the
[registration form](https://lkcamp.persona.ninja/limesurvey/index.php/135389)

NOTE: Meetings are held in Portuguese, but every body is welcome to chat in English through our mailing list or IRC/Telegram channel.

### Remote participation
- **Live stream:** [YouTube IC
  channel](https://www.youtube.com/channel/UCraCE6iWUcFCJSp-vmO1D3A/videos), [YouTube LKCAMP channel](https://www.youtube.com/channel/UC2skRId4WWg9F0vc6GAKRIA)
- **Mailing list:** lkcamp@lists.libreplanetbr.org - [subscribe
  here](https://lists.libreplanetbr.org/mailman/listinfo/lkcamp)
- **IRC Channel:** #lkcamp @ Freenode
- **Telegram (IRC bridge):** [https://t.me/lkcamp](https://t.me/lkcamp)

### Planned meetings - 2020

   * 10 Mar (tue):                    Workshop: sending your first contribution [video](https://www.youtube.com/watch?v=y6HxuJG1hQw)
   * 14 Apr (tue):                    Workshop: sending your first contribution (cont) [video](https://www.youtube.com/watch?v=_9tBWdj3m2o)
   * 23 Apr (**thu**):                    Git: crash course [video](https://youtu.be/mUjV22KXSQs)
   * 28 Apr (tue):                    Device Drivers [video](https://www.youtube.com/watch?v=qfqT7vEqoqo)
   *  5 May (tue):                    Syscalls [video](https://www.youtube.com/watch?v=QojgN0i1Mrg)
   * Weekly meetings:                 Advanced topics
   * TBD (sat/sun):                   LKCAMPING video4linux
   * Happy Hour (TBD)

![lkcamp-round4-flyer](imgs/lkcamp-divulgacao-round4.png)



