---
title: "Objetivos"
date: 2022-02-19T19:20:00-03:00
draft: false
---

Os objetivos do LKCAMP são:

- Disseminar conhecimentos do funcionamento e desenvolvimento do Kernel Linux ou
  outros projetos FOSS
- Ajudar pessoas interessadas a se tornarem contribuidoras do Kernel Linux ou
  outros projetos FOSS (principalmente no Brasil)
- Ajudar pessoas interessadas a serem remuneradas para trabalhar com o Kernel
  Linux ou outros projetos FOSS (principalmente no Brasil)

**Nota**: O LKCAMP surgiu no ambiente da Unicamp, e por isso existe bastante
interação com seus alunos, porém não é restrito a esse ambiente de forma alguma.
Todas as pessoas são mais que bem-vindas a participar.
