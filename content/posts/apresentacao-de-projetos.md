---
title: "Apresentação de Projetos"
date: 2023-05-26T20:46:20-03:00
draft: false
categories: ["category"]
tags: ["tag"]
authors: ["Pedro Sader Azevedo"]
---

# GNOME

O GNOME é um projeto que desenvolve uma série de tecnologias para o desktop
GNU/Linux, sendo o ambiente gráfico (*desktop environment*) GNOME Shell a mais
emblemática entre elas. Além disso, o GNOME mantém uma série de bibliotecas e
ferramentas de desenvolvimento de software, assim como um vasto ecossistema de
aplicativos.

**Tecnologias**: GTK, C, Javascript, Python, Rust

**Modelo de contribuição**: [GitLab](gitlab.gnome.org/)

**Comunicação do projeto**: [Matrix](https://wiki.gnome.org/GettingInTouch/Matrix/ExploringGnomeDirectory), [Discourse](https://discourse.gnome.org/)

**Tópico no Telegram do LKCAMP**: [\[GA\] GNOME](https://t.me/lkcamp/15257)

**Mentores do LKCAMP**:

- Pedro Sader Azevedo ([@pe_sader](https://t.me/pe_sader))
- Gustavo Montenari Pechta ([@GuPecz](https://t.me/GuPecz))

---

# Tradução

A tradução é essencial para ampliar o acesso à tecnologia, sendo fundamental
para garantir a "liberdade zero" do Software Livre: uso para qualquer
propósito. Além disso, contribuir com traduções pode ser menos intimidador
que contribuir com programação, por isso é uma excelente porta de entrada no
mundo FOSS.


**Tecnologias**: gettext, make, tecnologias web

**Modelo de contribuição**: Portais web (Weblate, DDTSS, etc), git forges

**Comunicação do projeto**: Depende do projeto traduzido

**Tópico no Telegram do LKCAMP**: [\[GA\] Tradução](https://t.me/lkcamp/15259)

**Mentores**:

- Pedro Sader Azevedo ([@pe_sader](https://t.me/pe_sader))
- Charles Melara ([@charles_melara](https://t.me/charles_melara))

---

# Projetos internos

O LKCAMP mantém uma série de projetos de código aberto, utilizados não apenas
por si mesmo mas também para outras entidades estudantis da Unicamp. Alguns
desses projetos são nosso site, nosso bot do telegram, e nossa ferramenta de
geração de certificados de participação em eventos.

**Tecnologias**: Hugo, Markdown, Python, GitLab CI

**Modelo de contribuição**: [GitLab](https://gitlab.com/lkcamp)

**Comunicação do projeto**: Grupo de Telegram do LKCAMP

**Tópico no Telegram do LKCAMP**: [\[GT\] Nils](https://t.me/lkcamp/15280), [Site](https://t.me/lkcamp/15252)

**Mentores**:

- Pedro Sader Azevedo ([@pe_sader](https://t.me/pe_sader))
- Ícaro Chiabai ([@archicarus](https://t.me/archicarus))

---
